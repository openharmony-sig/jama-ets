## jama单元测试用例

该测试用例基于OpenHarmony系统环境进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|constructor|pass||
|constructWithCopy|pass||
|copy|pass||
|clone|pass||
|getArray|pass||
|getArrayCopy|pass||
|getColumnPackedCopy|pass||
|getRowPackedCopy|pass||
|getRowDimension|pass||
|getColumnDimension|pass||
|get|pass||
|getMatrix$int$int$int$int|pass||
|getMatrix|pass||
|getMatrix$int$A$int$A|pass||
|getMatrix$int$int$int_A|pass||
|getMatrix$int_A$int$int|pass||
|set|pass||
|setMatrix$int$int$int$int$Jama_Matrix|pass||
|setMatrix|pass||
|setMatrix$int_A$int_A$Jama_Matrix|pass||
|setMatrix$int$A$int$int$Jama$Matrix|pass||
|setMatrix$int$int$int_A$Jama_Matrix|pass||
|transpose|pass||
|norm1|pass||
|norm2|pass||
|normInf|pass||
|normF|pass||
|uminus|pass||
|plus|pass||
|plusEquals|pass||
|minus|pass||
|minusEquals|pass||
|arrayTimes|pass||
|arrayTimesEquals|pass||
|arrayRightDivide|pass||
|arrayRightDivideEquals|pass||
|arrayLeftDivide|pass||
|arrayLeftDivideEquals|pass||
|times$double|pass||
|timesEquals|pass||
|times$Jama_Matrix|pass||
|times|pass||
|lu|pass||
|qr|pass||
|chol|pass||
|svd|pass||
|eig|pass||
|solve|pass||
|solveTranspose|pass||
|inverse|pass||
|det|pass||
|rank|pass||
|cond|pass||
|trace|pass||
|random|pass||
|identity|pass||
|checkMatrixDimensions|pass||