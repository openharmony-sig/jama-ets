## 2.0.2
- 在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)验证通过

## 2.0.1

- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs语法适配

## 2.0.0

- 包管理工具由npm切换为ohpm
- 适配DevEco Studio: 4.0 Canary2(4.0.1.300)
- 适配SDK: SDK: API10 (4.0.8.6)

## 1.0.2

- 适配DevEco Studio 3.1Beta1及以上版本。
- 适配OpenHarmony SDK API version 9及以上版本。

## v1.0.1

- api8升级到api9

## v1.0.0

- 已实现功能
  1. 对称正定矩阵分解

