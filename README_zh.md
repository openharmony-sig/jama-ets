# jama

## 简介
> 在OpenHarmony上支持矩阵运算、矩阵分解、线性代数等功能。例如构造和操作真实密集矩阵、计算矩阵的行列式等线性代数功能，为开发者提供了丰富的数学运算。

![preview.gif](preview/preview.gif)

## 下载安装
 ```shell
ohpm install @ohos/jama
 ```
OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明
1. 引入依赖
 ```
   import { Matrix } from '@ohos/jama'
 ```
2. 创建矩阵
 ```
    let array: number[][] = [[1., 2., 3], [4., 5., 6.], [7., 8., 10.]]
    let A: Matrix = new Matrix(array);
    let array2: number[][] = [[1,1,1],[1,1,1],[1,1,1]]
    let B: Matrix = new Matrix(array2);
    let x: Matrix = A.plus(B);
 ```

1. 矩阵加法
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.plus(B)
 ```
2. 矩阵减法
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.minus(B)
 ```
3. 矩阵乘法
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.times(B)
 ```
4. 矩阵左除除法
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.arrayLeftDivide(B)
 ```
5. 矩阵缩放
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.times(2) // 矩阵元素扩大2倍
 ```
6. 矩阵求逆
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.inverse() 
 ```
7. 矩阵转置
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.transpose() 
 ```
8. 矩阵范式
 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.cond() 
 ```

## 接口说明

| 类名                          | 方法名                                                         | 功能                |
|-----------------------------|-------------------------------------------------------------|-------------------|
| CholeskyDecomposition              | getL() : Matrix                                             | 返回三角因子            |
| CholeskyDecomposition                | isSPD() : boolean                                           | 该矩阵是否对称且正定        |
| CholeskyDecomposition                | solve(B : Matrix) : Matrix                                  | 求解 A*X = B        |
| LUDecomposition               | det() : number                                              | 行列式               |
| LUDecomposition                   | getDoublePivot() : number[]                                 | 以一维双精度数组形式返回枢轴排列向量 |
| LUDecomposition                  | getL() : Matrix                                             | 返回下三角因子           |
| LUDecomposition                  | getPivot() : number[]                                       | 返回枢轴置换向量          |
| LUDecomposition                  | getU() : Matrix                                             | 返回上三角因子           |
| LUDecomposition                 | isNonsingular() : boolean                                   | 该矩阵是否非奇异          |
| LUDecomposition                 | solve(B : Matrix) : Matrix                                  | 求解 A*X = B        |
| QRDecomposition           | getH() : Matrix                                             | 返回Householder矢量   |
| QRDecomposition           | getQ() : Matrix                                             | 生成并返回正交因子         |
| QRDecomposition           | getR() : Matrix                                             | 返回上三角因子           |
| QRDecomposition           | isFullRank() : boolean                                      | 是否是满秩的矩阵          |
| QRDecomposition           | solve(B : Matrix) : Matrix                                  | 求解 A*X = B          |
| EigenvalueDecomposition     | getD() : Matrix                                             | 返回块对角特征值矩阵        |
| EigenvalueDecomposition     | getV() : Matrix                                             | 返回特征向量矩阵          |
| EigenvalueDecomposition     | getRealEigenvalues() : number[]                             | 返回特征值的实部          |
| EigenvalueDecomposition     | getImagEigenvalues() : number[]                             | 返回特征值的虚部          |
| SingularValueDecomposition     | cond() : number                                             | 两个范数条件数         |
| SingularValueDecomposition     | getS() : Matrix                                             | 返回奇异值的对角矩阵          |
| SingularValueDecomposition     | getSingularValues() : number[]                              | 返回奇异值的一维数组          |
| SingularValueDecomposition     | getU() : Matrix                                             | 返回左奇异向量          |
| SingularValueDecomposition     | getV() : Matrix                                             | 返回右奇异向量          |
| SingularValueDecomposition     | norm2() : number                                            | 两个范数           |
| SingularValueDecomposition     | rank() : number                                             | 有效数值矩阵秩          |
| Matrix     | arrayLeftDivide(B : Matrix) : Matrix                        | 逐元素左分割，C=A\B        |
| Matrix     | arrayLeftDivideEquals(B : Matrix) : Matrix                  | 一个元素接一个元素，左分割到位，A=A\B          |
| Matrix     | arrayRightDivide(B : Matrix) : Matrix                       | 逐个元素的右划分，C=A./B          |
| Matrix     | arrayRightDivideEquals(B : Matrix) : Matrix                 | 逐元素右划分到位，A=A./B          |
| Matrix     | arrayTimes(B : Matrix) : Matrix                             | 逐元素相乘，C=A.*B          |
| Matrix     | arrayTimesEquals(B : Matrix) : Matrix                       | 逐元素相乘到位，A=A.*B          |
| Matrix     | chol() : CholeskyDecomposition                              | Cholesky分解          |
| Matrix     | clone() : any                                               | 克隆Matrix对象。        |
| Matrix     | constructWithCopy(A : number[][]) : Matrix                  | 从二维数组的副本构造矩阵。          |
| Matrix     | copy() : Matrix                                             | 深入复制矩阵          |
| Matrix     | det() : number                                              | 返回左奇异向量          |
| Matrix     | eig() : EigenvalueDecomposition                             | 特征值分解           |
| Matrix     | get(i : number, j : number) : number                        | 获取单个元素          |
| Matrix     | getArray() : number[][]                                     | 访问内部二维数组          |
| Matrix     | getArrayCopy() : number[][]                                 | 复制内部二维数组        |
| Matrix     | getColumnDimension() : number                               | 获取列维度           |
| Matrix     | getColumnPackedCopy() : number[]                            | 制作内部数组的一维列压缩副本          |
| Matrix     | getMatrix(i0? : any, i1? : any, j0? : any, j1? : any) : any | 获取一个子矩阵。          |
| Matrix     | getRowDimension() : number                                  | 获取行维度。          |
| Matrix     | getRowPackedCopy() : number[]                               | 制作内部数组的一维行压缩副本           |
| Matrix     | identity(m : number, n : number) : Matrix                   | 生成身份矩阵          |
| Matrix     | inverse() : Matrix                                          | 矩阵逆或伪逆         |
| Matrix     | lu() : LUDecomposition                                      | LU分解          |
| Matrix     | minus(B : Matrix) : Matrix                                  | C = A - B          |
| Matrix     | minusEquals(B : Matrix) : Matrix                            | A = A - B         |
| Matrix     | norm1() : number                                            | 一个范数           |
| Matrix     | norm2() : number                                            | 两个范数           |
| Matrix     | normF() : number                                            | Frobenius范数          |
| Matrix     | arrayLeftDivide(B : Matrix) : Matrix                        | 逐元素左分割，C=A\B        |
| Matrix     | arrayLeftDivideEquals(B : Matrix) : Matrix                  | 一个元素接一个元素，左分割到位，A=A\B          |
| Matrix     | arrayRightDivide(B : Matrix) : Matrix                       | 逐个元素的右划分，C=A./B          |
| Matrix     | arrayRightDivideEquals(B : Matrix) : Matrix                 | 逐元素右划分到位，A=A./B          |
| Matrix     | arrayTimes(B : Matrix) : Matrix                             | 逐元素相乘，C=A.*B          |
| Matrix     | arrayTimesEquals(B : Matrix) : Matrix                       | 逐元素相乘到位，A=A.*B          |
| Matrix     | chol() : CholeskyDecomposition                              | Cholesky分解          |
| Matrix     | clone() : any                                               | 克隆Matrix对象。        |
| Matrix     | constructWithCopy(A : number[][]) : Matrix                  | 从二维数组的副本构造矩阵。          |
| Matrix     | copy() : Matrix                                             | 深入复制矩阵          |
| Matrix     | det() : number                                              | 返回左奇异向量          |
| Matrix     | eig() : EigenvalueDecomposition                             | 特征值分解           |
| Matrix     | get(i : number, j : number) : number                        | 获取单个元素          |
| Matrix     | getArray() : number[][]                                     | 访问内部二维数组          |
| Matrix     | getArrayCopy() : number[][]                                 | 复制内部二维数组        |
| Matrix     | getColumnDimension() : number                               | 获取列维度           |
| Matrix     | getColumnPackedCopy() : number[]                            | 制作内部数组的一维列压缩副本          |
| Matrix     | getMatrix(i0? : any, i1? : any, j0? : any, j1? : any) : any | 获取一个子矩阵。          |
| Matrix     | getRowDimension() : number                                  | 获取行维度。          |
| Matrix     | getRowPackedCopy() : number[]                               | 制作内部数组的一维行压缩副本           |
| Matrix     | identity(m : number, n : number) : Matrix                   | 生成身份矩阵          |
| Matrix     | inverse() : Matrix                                          | 矩阵逆或伪逆         |
| Matrix     | lu() : LUDecomposition                                      | LU分解          |
| Matrix     | minus(B : Matrix) : Matrix                                  | C = A - B          |
| Matrix     | minusEquals(B : Matrix) : Matrix                            | A = A - B         |
| Matrix     | norm1() : number                                            | 一个范数           |
| Matrix     | norm2() : number                                            | 两个范数           |
| Matrix     | normF() : number                                            | Frobenius范数          |
| Matrix     | normInf() : number                                          | 无穷范数          |
| Matrix     | plus(B : Matrix) : Matrix                                   | C = A + B          |
| Matrix     | plusEquals(B : Matrix) : Matrix                             | A = A + B           |
| Matrix     | qr() : QRDecomposition                                      | QR分解          |
| Matrix     | random(m : number, n : number) : Matrix                     | 生成具有随机元素的矩阵          |
| Matrix     | rank() : number                                             | 矩阵秩        |
| Matrix     | set(i : number, j : number, s : number):void                | 设置单个元素           |
| Matrix     | setMatrix(i0? : any, i1? : any, j0? : any, j1? : any, X? : any) : any                            | 设置一个子矩阵。          |
| Matrix     | solve(B : Matrix) : Matrix | 求解A*X=B          |
| Matrix     | solveTranspose(B : Matrix) : Matrix                                  | 求解X*A=B，也就是A'*X'=B'          |
| Matrix     | svd() : SingularValueDecomposition                               | 奇异值分解          |
| Matrix     | times(B? : any) : any                   | 线性代数矩阵乘法，A*B          |
| Matrix     | timesEquals(s : number) : Matrix                                          | 将矩阵乘以一个标量，a=s*a         |
| Matrix     | trace() : number                                      | 矩阵跟踪          |
| Matrix     | transpose() : Matrix                                  | 矩阵转置          |
| Matrix     | uminus() : Matrix                            | 减号        |

## 约束与限制

在下述版本验证通过：

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 (5.0.0.66)

## 目录结构
 ````
|---- jama
|    |---- entry  # 示例代码文件夹
|    |---- library  # jama库文件夹
|        |---- src
|            |---- main
|                |---- ets
|                    |---- components
|                        |---- util  # 工具类文件
|                            |---- Maths.ts  # 指数计算类
|                        |---- CholeskyDecomposition.ts  # 乔列斯基分解类
|                        |---- EigenvalueDecomposition.ts  # 矩阵的特征值和特征向量
|                        |---- LUDecomposition.ts  # LU分解
|                        |---- Matrix.ts  # 矩阵类
|                        |---- QRDecomposition.ts  # QR分解
|                        |---- SingularValueDecomposition.ts  # 奇异值分解
|        |---- index.ets  # 对外接口
|    |---- README.md  # 安装使用方法                    
|    |---- README_zh.md  # 安装使用方法                    
 ````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/jama/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/jama/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/jama/blob/master/LICENSE) ，请自由地享受和参与开源。
