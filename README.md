# jama

## Introduction
**jama** allows matrix operations, matrix decomposition, and linear algebra on OpenHarmony. You can use the APIs provided by **jama** to implement linear algebra operations such as constructing and operating a real dense matrix and calculating the determinant of a matrix.

![preview.gif](preview/preview.gif)

## How to Install
 ```shell
ohpm install @ohos/jama
 ```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use
1. Import dependencies.
 ```
   import { Matrix } from '@ohos/jama'
 ```
2. Create matrix A and matrix B.
 ```
    let array: number[][] = [[1., 2., 3], [4., 5., 6.], [7., 8., 10.]]
    let A: Matrix = new Matrix(array);
    let array2: number[][] = [[1,1,1],[1,1,1],[1,1,1]]
    let B: Matrix = new Matrix(array2);
    let x: Matrix = A.plus(B);
 ```

3. Add matrix A and matrix B.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.plus(B)
 ```
4. Subtract matrix B from matrix A.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.minus(B)
 ```
5. Multiply matrix A with matrix B.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.times(B)
 ```
6. Perform element-wise division of matrix A and matrix B.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let array2: number[][] = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
   let B: Matrix = new Matrix(array2);
   let C = A.arrayLeftDivide(B)
 ```
7. Multiply a matrix.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.times(2) // The matrix is multiplied by 2 times.
 ```
8. Calculate the inverse of a matrix.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.inverse() 
 ```
9. Transpose a matrix.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.transpose() 
 ```
10. Calculate the condition number of a matrix.

 ```
   let array: number[][] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   let A: Matrix = new Matrix(array);
   let C = A.cond() 
 ```

## Available APIs

| Class                         | API                                                        | Description               |
|-----------------------------|-------------------------------------------------------------|-------------------|
| CholeskyDecomposition              | getL() : Matrix                                             | Obtains the triangular matrix from a decomposition.       |
| CholeskyDecomposition                | isSPD() : boolean                                           | Checks whether the matrix is symmetric positive definite.       |
| CholeskyDecomposition                | solve(B : Matrix) : Matrix                                  | Solves the linear system (AX = B) for (X).       |
| LUDecomposition               | det() : number                                              | Returns the determinant of the matrix.              |
| LUDecomposition                   | getDoublePivot() : number[]                                 | Obtains the pivot indices as a double array. |
| LUDecomposition                  | getL() : Matrix                                             | Obtains the lower triangular matrix from a decomposition.   |
| LUDecomposition                  | getPivot() : number[]                                       | Obtains the pivot indices.  |
| LUDecomposition                  | getU() : Matrix                                             | Obtains the upper triangular matrix from a decomposition.   |
| LUDecomposition                 | isNonsingular() : boolean                                   | Checks whether the matrix is non-singular.         |
| LUDecomposition                 | solve(B : Matrix) : Matrix                                  | Solves the linear system (AX = B) for (X).       |
| QRDecomposition           | getH() : Matrix                                             | Obtains the Householder vector. |
| QRDecomposition           | getQ() : Matrix                                             | Obtains the orthogonal matrix (Q) from a decomposition. |
| QRDecomposition           | getR() : Matrix                                             | Obtains the upper triangular matrix (R) from a decomposition.   |
| QRDecomposition           | isFullRank() : boolean                                      | Checks whether the matrix has full rank.         |
| QRDecomposition           | solve(B : Matrix) : Matrix                                  | Solves the linear system (AX = B) for (X).         |
| EigenvalueDecomposition     | getD() : Matrix                                             | Obtains the diagonal matrix (D) from a decomposition. |
| EigenvalueDecomposition     | getV() : Matrix                                             | Obtains the matrix (V).  |
| EigenvalueDecomposition     | getRealEigenvalues() : number[]                             | Obtains the real parts of the eigenvalues.  |
| EigenvalueDecomposition     | getImagEigenvalues() : number[]                             | Obtains the imaginary parts of the eigenvalues.  |
| SingularValueDecomposition     | cond() : number                                             | Returns the condition number of the matrix.        |
| SingularValueDecomposition     | getS() : Matrix                                             | Obtains the singular values as a diagonal matrix (S) from the Singular Value Decomposition (SVD).         |
| SingularValueDecomposition     | getSingularValues() : number[]                              | Obtains the singular values as an array.         |
| SingularValueDecomposition     | getU() : Matrix                                             | Obtains the U matrix, which is part of the LU decomposition of a matrix.          |
| SingularValueDecomposition     | getV() : Matrix                                             | Obtains the V matrix, which is part of the SVD of a matrix.         |
| SingularValueDecomposition     | norm2() : number                                            | Returns the 2-norm (largest singular value) of the matrix.          |
| SingularValueDecomposition     | rank() : number                                             | Returns the rank of the matrix.         |
| Matrix     | arrayLeftDivide(B : Matrix) : Matrix                        | Performs element-wise division of the current matrix by matrix (B), that is, C = A\B.       |
| Matrix     | arrayLeftDivideEquals(B : Matrix) : Matrix                  | Performs element-wise division of the current matrix by matrix (B) and updates the current matrix with the result, that is, A = A\B.         |
| Matrix     | arrayRightDivide(B : Matrix) : Matrix                       | Performs element-wise division of matrix (B) by the current matrix, that is, C = A./B.         |
| Matrix     | arrayRightDivideEquals(B : Matrix) : Matrix                 | Performs element-wise division of matrix (B) by the current matrix and updates the current matrix with the result, that is, A = A./B.         |
| Matrix     | arrayTimes(B : Matrix) : Matrix                             | Performs element-wise multiplication of the current matrix by matrix B, that is, C = A. x B.         |
| Matrix     | arrayTimesEquals(B : Matrix) : Matrix                       | Performs element-wise multiplication of the current matrix by matrix B and updates the current matrix with the result, that is, A = A. x B.         |
| Matrix     | chol() : CholeskyDecomposition                              | Performs Cholesky decomposition of the matrix.         |
| Matrix     | clone() : any                                               | Clones a matrix object.       |
| Matrix     | constructWithCopy(A : number[][]) : Matrix                  | Constructs a matrix from a 2D array of numbers.         |
| Matrix     | copy() : Matrix                                             | Returns a copy of the matrix.         |
| Matrix     | det() : number                                              | Returns the determinant of the matrix.         |
| Matrix     | eig() : EigenvalueDecomposition                             | Performs eigenvalue decomposition of the matrix.          |
| Matrix     | get(i : number, j : number) : number                        | Obtains the element at the specified row and column.         |
| Matrix     | getArray() : number[][]                                     | Obtains the matrix as a 2D array.         |
| Matrix     | getArrayCopy() : number[][]                                 | Obtains a copy of the matrix as a 2D array.       |
| Matrix     | getColumnDimension() : number                               | Obtains the number of columns in the matrix.          |
| Matrix     | getColumnPackedCopy() : number[]                            | Obtains a 1D array packed by columns.         |
| Matrix     | getMatrix(i0? : any, i1? : any, j0? : any, j1? : any) : any | Obtains a submatrix.         |
| Matrix     | getRowDimension() : number                                  | Obtains the number of rows in the matrix.         |
| Matrix     | getRowPackedCopy() : number[]                               | Obtains a 1D array packed by rows.          |
| Matrix     | identity(m : number, n : number) : Matrix                   | Returns an identity matrix.         |
| Matrix     | inverse() : Matrix                                          | Returns the inverse of the matrix.        |
| Matrix     | lu() : LUDecomposition                                      | Performs LU decomposition of the matrix.         |
| Matrix     | minus(B : Matrix) : Matrix                                  | C = A - B          |
| Matrix     | minusEquals(B : Matrix) : Matrix                            | A = A - B         |
| Matrix     | norm1() : number                                            | Returns the 1-norm (maximum column sum) of the matrix.          |
| Matrix     | norm2() : number                                            | Returns the 2-norm (largest singular value) of the matrix.          |
| Matrix     | normF() : number                                            | Returns the Frobenius norm.         |
| Matrix     | arrayLeftDivide(B : Matrix) : Matrix                        | Performs element-wise division of the current matrix by matrix (B), that is, C = A\B.       |
| Matrix     | arrayLeftDivideEquals(B : Matrix) : Matrix                  | Performs element-wise division of the current matrix by matrix (B) and updates the current matrix with the result, that is, A = A\B.         |
| Matrix     | arrayRightDivide(B : Matrix) : Matrix                       | Performs element-wise division of matrix (B) by the current matrix, that is, C = A./B.         |
| Matrix     | arrayRightDivideEquals(B : Matrix) : Matrix                 | Performs element-wise division of matrix (B) by the current matrix and updates the current matrix with the result, that is, A = A./B.         |
| Matrix     | arrayTimes(B : Matrix) : Matrix                             | Performs element-wise multiplication of the current matrix by matrix B, that is, C = A. x B.         |
| Matrix     | arrayTimesEquals(B : Matrix) : Matrix                       | Performs element-wise multiplication of the current matrix by matrix B and updates the current matrix with the result, that is, A = A. x B.         |
| Matrix     | chol() : CholeskyDecomposition                              | Performs Cholesky decomposition of the matrix.         |
| Matrix     | clone() : any                                               | Clones a matrix object.       |
| Matrix     | constructWithCopy(A : number[][]) : Matrix                  | Constructs a matrix from a 2D array of numbers.         |
| Matrix     | copy() : Matrix                                             | Returns a copy of the matrix.         |
| Matrix     | det() : number                                              | Returns the determinant of the matrix.         |
| Matrix     | eig() : EigenvalueDecomposition                             | Performs eigenvalue decomposition of the matrix.          |
| Matrix     | get(i : number, j : number) : number                        | Obtains the element at the specified row and column.         |
| Matrix     | getArray() : number[][]                                     | Obtains the matrix as a 2D array.         |
| Matrix     | getArrayCopy() : number[][]                                 | Obtains a copy of the matrix as a 2D array.       |
| Matrix     | getColumnDimension() : number                               | Obtains the number of columns in the matrix.          |
| Matrix     | getColumnPackedCopy() : number[]                            | Obtains a 1D array packed by columns.         |
| Matrix     | getMatrix(i0? : any, i1? : any, j0? : any, j1? : any) : any | Obtains a submatrix.         |
| Matrix     | getRowDimension() : number                                  | Obtains the number of rows in the matrix.         |
| Matrix     | getRowPackedCopy() : number[]                               | Obtains a 1D array packed by rows.          |
| Matrix     | identity(m : number, n : number) : Matrix                   | Returns an identity matrix.         |
| Matrix     | inverse() : Matrix                                          | Returns the inverse of the matrix.        |
| Matrix     | lu() : LUDecomposition                                      | Performs LU decomposition of the matrix.         |
| Matrix     | minus(B : Matrix) : Matrix                                  | C = A - B          |
| Matrix     | minusEquals(B : Matrix) : Matrix                            | A = A - B         |
| Matrix     | norm1() : number                                            | Returns the 1-norm (maximum column sum) of the matrix.          |
| Matrix     | norm2() : number                                            | Returns the 2-norm (largest singular value) of the matrix.          |
| Matrix     | normF() : number                                            | Returns the Frobenius norm.         |
| Matrix     | normInf() : number                                          | Returns the infinity norm of the matrix.         |
| Matrix     | plus(B : Matrix) : Matrix                                   | C = A + B          |
| Matrix     | plusEquals(B : Matrix) : Matrix                             | A = A + B           |
| Matrix     | qr() : QRDecomposition                                      | Performs QR decomposition of the matrix.         |
| Matrix     | random(m : number, n : number) : Matrix                     | Creates a matrix with random elements.         |
| Matrix     | rank() : number                                             | Returns the rank of the matrix.       |
| Matrix     | set(i : number, j : number, s : number):void                | Sets a single element.          |
| Matrix     | setMatrix(i0? : any, i1? : any, j0? : any, j1? : any, X? : any) : any                            | Sets a submatrix.         |
| Matrix     | solve(B : Matrix) : Matrix | Solves the linear system (AX = B) for (X).         |
| Matrix     | solveTranspose(B : Matrix) : Matrix                                  | Solves the system XA=B, that is, A'X'=B'.         |
| Matrix     | svd() : SingularValueDecomposition                               | Calculates the SVD of the matrix.         |
| Matrix     | times(B? : any) : any                   | Multiplies matrix A by another matrix B.         |
| Matrix     | timesEquals(s : number) : Matrix                                          | Multiplies a matrix by a scalar, where a = s x a.        |
| Matrix     | trace() : number                                      | Returns the trace of the matrix.         |
| Matrix     | transpose() : Matrix                                  | Returns the transpose of the matrix.         |
| Matrix     | uminus() : Matrix                            | Returns the negation of the matrix.       |

## Constraints

This project has been verified in the following version:

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 (5.0.0.66)

## Directory Structure
 ````
|---- jama
|     |---- entry  # Sample code
|    |---- library  # jama library
|        |---- src
|            |---- main
|                |---- ets
|                    |---- components
|                        |---- util  # Utility
|                            |---- Maths.ts  # Exponential calculation
|                        |---- CholeskyDecomposition.ts    # Cholesky decomposition
|                        |---- EigenvalueDecomposition.ts  # Eigenvalue decomposition
|                        |---- LUDecomposition.ts          # LU decomposition
|                        |---- Matrix.ts                   # Matrix operations
|                        |---- QRDecomposition.ts          # QR decomposition
|                        |---- SingularValueDecomposition.ts  # Singular value decomposition
|        |---- index.ets  # External APIs
|     |---- README.md  # Readme                   
|     |---- README_zh.md  # Readme                   
 ````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/jama/issues) or a [PR](https://gitee.com/openharmony-sig/jama/pulls).

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/jama/blob/master/LICENSE).
